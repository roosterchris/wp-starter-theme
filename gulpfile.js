'use strict';

/*
	Variables
	- requires for Gulp
	- postCSS settings
	- paths
	- styleguide settings
 */

var gulp = require('gulp');
var sass = require('gulp-sass');
var terser = require('gulp-terser');
var imagemin = require('gulp-imagemin');
var flatten = require('gulp-flatten');
var postcss = require('gulp-postcss');
var modernizr = require('gulp-modernizr');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var phpcs = require('gulp-phpcs');
var jquery = 'node_modules/jquery/dist/jquery.min.js';
var slickSliderJs = 'node_modules/slick-slider/slick/slick.js';
var slickSliderCss = 'node_modules/slick-slider/slick/slick.scss';
var slickSliderThemeCss = 'node_modules/slick-slider/slick/slick-theme.scss';
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var phpcbf = require('gulp-phpcbf');
var sourcemaps = require('gulp-sourcemaps');

// var isotope = 'node_modules/isotope-layout/dist/isotope.pkgd.js';

// var fancyBoxJs = 'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js';
// var fancyBoxCss = 'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.css';

var objectFitJs = 'node_modules/object-fit-images/dist/ofi.js';

var postcssProcessorsDev = [
	require('autoprefixer'),
	require('postcss-object-fit-images')
];
var postcssProcessorsProd = [
	require('autoprefixer'),
	require('cssnano')(),
	require('postcss-object-fit-images')
];

var paths = {
	// srcFiles: './src/',
	srcJsRoot: './src/js/',
	srcLibrariesJsRoot: './src/js/libraries/',
	srcSassRoot: './src/sass/',
	srcJsAll: './src/js/**/*.js',
	srcJsDev: ['./src/js/**/*.js', '!./src/js/libraries/*.js'],
	srcLibrariesJs: './src/js/libraries/*.js',
	srcSass: ['./src/sass/**/*.scss', '!./src/sass/import-maps/*.scss'],
	srcSassAll: './src/sass/**/*.scss',
	srcImages: './src/img/**/*',
	phpFiles: ['**/*.php', '!bower_components/**/*.*', '!node_modules/**/*.*', '!reports/**/*.*', '!wpcs/**/*.*', '!acf-json/**/*.*', '!vendor/**/*.*'],
	distImages: './dist/img/',
	distCss: './dist/css/',
	distJs: './dist/js/',
	distLibrariesJs: './dist/js/libraries/',
};

/**
	CSS / Sass tasks
	- sass:dev - the watch task (readable css)
	- sass:prod - the build task (compressed css)
**/

gulp.task('browser-sync', function(cb) {
    browserSync.init({
			open: 'external',
			host: 'SITESLUG.local',
			proxy: 'https://SITESLUG.local',
			https: {
				key: "/Users/chrisrooster/Library/Application Support/Local/run/router/nginx/certs/SITESLUG.local.key",
        cert: "/Users/chrisrooster/Library/Application Support/Local/run/router/nginx/certs/SITESLUG.local.crt"
			},

			watchOptions: {
	      debounceDelay: 1000 // This introduces a small delay when watching for file change events to avoid triggering too many reloads
	    }
    });
			// Watch PHP and CSS files.
		gulp.watch(paths.phpFiles).on("change", reload)
		gulp.watch(paths.srcSassAll).on("change", reload)
		gulp.watch(paths.srcJsAll).on("change", reload);

		cb();
});


gulp.task('sass:dev', function (cb) {
	gulp.src(paths.srcSass)
		.pipe(sass({
			outputStyle: 'expanded',
			errLogToConsole: true
		}))
		.pipe(postcss(postcssProcessorsDev))
		.pipe(gulp.dest(paths.distCss))
		.pipe(browserSync.stream());
	// Callback
	cb();
});

gulp.task('sass:prod', function (cb) {
	gulp.src(paths.srcSass)
		.pipe(sass({
			outputStyle: 'compressed',
			errLogToConsole: true
		}))
		.pipe(postcss(postcssProcessorsProd))
		.pipe(gulp.dest(paths.distCss));
	// Callback
	cb();
});

/**
	Javascript tasks
	- js:dev - the watch task (readable js)
	- js:prod - the build task (compressed js)
	- modernizr - builds a custom modernizr with some preset tests and whatever's in the css/js files
	- concat:js - conact task for the 3rd party files
**/

gulp.task('vendor', function (cb) {
	// jQuery
	gulp.src(jquery)
		.pipe( gulp.dest(paths.srcJsRoot) );

	// Slick Slider
	gulp.src(slickSliderJs)
		.pipe( gulp.dest(paths.srcLibrariesJsRoot) );

	gulp.src(slickSliderCss)
		.pipe(rename('_core.scss'))
		.pipe( gulp.dest(paths.srcSassRoot + 'slickslider/') );

	gulp.src(slickSliderThemeCss)
		.pipe(rename('_theme.scss'))
		.pipe( gulp.dest(paths.srcSassRoot + 'slickslider/') );

	// Isotope
	// gulp.src(isotope)
	// 	.pipe(rename('isotope.js'))
	// 	.pipe( gulp.dest(paths.srcLibrariesJsRoot) );

	// Fancybox
	// gulp.src(fancyBoxJs)
	// 	.pipe(rename('fancybox.js'))
	// 	.pipe( gulp.dest(paths.srcLibrariesJsRoot) );
	//
	// gulp.src(fancyBoxCss)
	// 	.pipe(rename('_core.scss'))
	// 	.pipe( gulp.dest(paths.srcSassRoot + 'fancybox/') );

	// Object Fit Images
	gulp.src(objectFitJs)
		.pipe(rename('object-fit-images.js'))
		.pipe( gulp.dest(paths.srcLibrariesJsRoot) );

 	cb();
});

gulp.task('js:dev', function (cb) {
	gulp.src(paths.srcJsDev)
		// .pipe(flatten())
		.pipe(gulp.dest(paths.distJs))
		.pipe(browserSync.stream());
	// Callback
	cb();
});

gulp.task('js:prod', function (cb) {
	return gulp.src(paths.srcJsDev)
		.pipe(terser())
		.pipe(gulp.dest(paths.distJs));
	// Callback
	cb();
});

gulp.task('modernizr', function() {
  return gulp.src([paths.srcJsAll,paths.srcSassAll])
    .pipe(modernizr(
			{
				'options': ['setClasses', 'html5printshiv'],
				'tests': ['objectfit', 'flexbox', 'flexboxlegacy', 'flexboxtweener']
			}
		))
    .pipe(gulp.dest(paths.distJs))
});

gulp.task('concat:js', function (cb) {
	gulp.src([
		paths.srcLibrariesJs,
	])
	.pipe(concat('bundle.js'))
	.pipe(gulp.dest(paths.distLibrariesJs));
	// Callback
	cb();
});

/**
	PHP tasks
	- php:lint - linting the php, runs PHPCS using the WordPress standard with the following overrides:
		- WordPress.PHP.YodaConditions - because pointless Yoda conditionals mostly are
		- WordPress.WhiteSpace.DisallowInlineTabs - tabs are better for indenting arrays
		- WordPress.WP.EnqueuedResources - enqueing scripts is less important than performance on custom themes
**/

gulp.task('php:lint', function () {
	return gulp.src(paths.phpFiles)
	// Validate files using PHP Code Sniffer
	.pipe(phpcs({
		bin: 'vendor/bin/phpcs',
		standard: 'WordPress',
		exclude: ['WordPress.PHP.YodaConditions', 'WordPress.WhiteSpace.DisallowInlineTabs', 'WordPress.WP.EnqueuedResources', 'WordPress.Files.FileName'],
		warningSeverity: 0
	}))
	// Log all problems that was found
	.pipe(phpcs.reporter('log'))
});

gulp.task('php:fix', function (cb) {
  return gulp.src(paths.phpFiles)
  .pipe(phpcbf({
		bin: 'vendor/bin/phpcbf',
		standard: 'WordPress',
		exclude: ['WordPress.PHP.YodaConditions', 'WordPress.WhiteSpace.DisallowInlineTabs', 'WordPress.WP.EnqueuedResources', 'WordPress.Files.FileName'],
		warningSeverity: 0

  }))
  .pipe(gulp.dest('./'))
	// .pipe(browserSync.stream());

	cb();
});

/**
	Image tasks
	- imagemin - moves & minifies images from src to dist
**/

gulp.task('imagemin', function (cb) {
	gulp.src(paths.srcImages)
		.pipe(imagemin())
		.pipe(gulp.dest(paths.distImages))
		.pipe(browserSync.stream());
	// Callback
	cb();
});

/**
	Watch
	- This defines which files to watch and what functions to run when they change
**/

function watchFiles() {
	gulp.watch(paths.srcSass, gulp.series('sass:dev')); // Create the css, lint the sass, update the styleguide
	gulp.watch(paths.srcJsAll, gulp.series('js:dev', 'modernizr', 'concat:js'));
	gulp.watch(paths.srcImages, gulp.series('imagemin')); // Compress all images
	gulp.watch(paths.phpFiles, gulp.series('php:fix', 'php:lint')); // Lint php files

}

gulp.task("watch", gulp.parallel(watchFiles));


/**
	Global tasks
	- Default / watch
	- Build
**/

gulp.task('default', gulp.series(
	'browser-sync',
	gulp.parallel('watch'),
));

gulp.task('build', gulp.series(
	'sass:prod',
	'js:prod',
	gulp.parallel('php:lint', 'modernizr', 'imagemin'),
));
