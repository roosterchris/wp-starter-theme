<?php
/**
 * Template Name: Flexible Template
 *
 * @package NAMEOFTHEME
 */

 use Rooster\NAMEOFSLUG as Theme;

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/flexible', 'content' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
