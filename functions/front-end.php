<?php
/**
 * Frontend functions for the CLIENTNAME website theme
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFSLUG;

use WPCF7_FormTag;

/**
 * ===========================================================================================================================================
 * Responsive Image Helper Function
 *
 * @param string $image_id The id of the image (from ACF or similar).
 * @param string $image_size The size of the thumbnail image or custom image size.
 * @param string $max_width The max width this image will be shown to build the sizes attribute.
 */
function responsive_image( $image_id, $image_size, $max_width, $screen_width = null ) {
	// check the image ID is not blank.
	if ( $image_id != '' ) {
		// set the default src image size.
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes.
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		// set the default image metadata.
		$image_meta = wp_get_attachment_metadata( $image_id );

		// set the image attachment to the correct size to pull out the width and height atttributes.
		if ( $image_meta['sizes'] ) {
			$image_attr = $image_meta['sizes'][ $image_size ];
		} else {
			$image_attr = wp_get_attachment_metadata( $image_id );
		}

		if ( $screen_width != '' ) {
			$viewport_width = $screen_width;
		} else {
			$viewport_width = '100vw';
		}
		// generate the markup for the responsive image.
		echo 'width="' . esc_html( $image_attr['width'] ) . '" height="' . esc_html( $image_attr['height'] ) . '"';
		echo 'src="' . esc_attr( $image_src ) . '" srcset="' . esc_attr( $image_srcset ) . '" sizes="(max-width: ' . esc_attr( $max_width ) . ') ' . esc_html( $viewport_width ) . ', ' . esc_attr( $max_width ) . '"';
	}
}


/**
 * ===========================================================================================================================================
 * Hide the Admin Bar when on the front end of the website.
 */
add_filter( 'show_admin_bar', '__return_false' );

/**
 * ===========================================================================================================================================
 * Change the length of the excerpt.
 *
 * @param string $length The length of the excerpt.
 */
function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', __NAMESPACE__ . '\custom_excerpt_length', 999 );

/**
 * ===========================================================================================================================================
 * Custom WP Login functions
 */
require get_template_directory() . '/functions/inc/custom-login.php';

/**
 * ===========================================================================================================================================
 * Remove Query Strings
 *
 * @param string $url The url for asyncloading.
 */
function _remove_script_version( $url ) {
	$parts = explode( '?ver', $url );

	// If there is an #asyncload in the url this will allow it to strip out just the ?ver.
	if ( strpos( $url, '#asyncload' ) !== false ) {
		$url = str_replace( '#asyncload', '', $url ) . "' async='async";

		$async_string = $parts[0] . '#asyncload';

		return $async_string;
	}

	return $parts[0];
}
add_filter( 'script_loader_src', __NAMESPACE__ . '\_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', __NAMESPACE__ . '\_remove_script_version', 15, 1 );

/**
 * ===========================================================================================================================================
 * Google Maps
 *
 * Return the google API key, also used in the loading of the google JS.
 */
function google_api_key() {
	return 'AIzaSyBKSmXoBDv8wAWZB21PEXyyl-_XJL0dars';
}

/**
 * Filter ACF's Google Map field to apply a valid API key.
 */
function acf_init() {
	$the_key = google_api_key();
	acf_update_setting( 'google_api_key', $the_key );
}
add_action( 'acf/init', __NAMESPACE__ . '\acf_init' );

/**
 * ===========================================================================================================================================
 * Edit HTML of post navigation
 *
 * @param string $matches Which navigation page you are on.
 */
function edited_post_navigation( $matches ) {
	if ( 'prev' == $matches[1] ) {
		$np = 'Previous';
	} else {
		$np = 'Next';
	}

	$ret = 'rel="' . $matches[1] . '" class="btn" ';

	return $ret;
}

/**
 * Add the attributes to the links
 *
 * @param string $link Is just the link.
 */
function add_attr_to_np_links( $link ) {
	return preg_replace_callback( '/rel="([^"]+)"/', __NAMESPACE__ . '\edited_post_navigation', $link );
}
add_filter( 'next_post_link', __NAMESPACE__ . '\add_attr_to_np_links' );
add_filter( 'previous_post_link', __NAMESPACE__ . '\add_attr_to_np_links' );

/**
 * ===========================================================================================================================================
 * Print SVG code from image tag
 *
 * @param string $svg_url The url for the svg path.
 */
function print_svg_file( $svg_url ) {
	echo file_get_contents( $svg_url );
}

/**
 * ===========================================================================================================================================
 * Custom Browser Functions
 */
require get_template_directory() . '/functions/inc/browser-functions.php';

/**
 * ===========================================================================================================================================
 * Call the meta title for the current post
 *
 * @param string $post_id The ID for the post.
 */
function yoast_variable_to_title( $post_id ) {
	$yoast_title = get_post_meta( $post_id, '_yoast_wpseo_title', true );

	$title = strstr( $yoast_title, '%%', true );

	if ( empty( $title ) ) {
		$title = get_the_title( $post_id );
	}

	$wpseo_titles = get_option( 'wpseo_titles' );

	$sep_options = WPSEO_Option_Titles::get_instance()->get_separator_options();

	if ( isset( $wpseo_titles['separator'] ) && isset( $sep_options[ $wpseo_titles['separator'] ] ) ) {
		$sep = $sep_options[ $wpseo_titles['separator'] ];
	} else {
		$sep = '-'; // setting default separator if Admin didn't set it from backed.
	}

	$site_title = get_bloginfo( 'name' );

	$meta_title = $title . ' ' . $sep . ' ' . $site_title;

	return $meta_title;
}

/**
 * ===========================================================================================================================================
 * Remove sticky post from the loop, only keeping it at the top of the first page
 */
add_action(
	'pre_get_posts',
	function( $q ) {
		if ( $q->is_home() && $q->is_main_query() && $q->get( 'paged' ) > 1 ) {
			$q->set( 'post__not_in', get_option( 'sticky_posts' ) );
		}
	}
);

/**
 * ===========================================================================================================================================
 * Custom Contact Form 7 submit button
 *
 * @param string $tag The text for the button is submit.
 */
function wpcf7_submit_button_tag_handler( $tag ) {

	$tag = new WPCF7_FormTag( $tag );
	$atts = array();

	$atts['id'] = $tag->get_id_option();
	$atts['tabindex'] = $tag->get_option( 'tabindex', 'int', true );
	$value = isset( $tag->values[0] ) ? $tag->values[0] : '';

	if ( empty( $value ) ) {

		$value = __( 'Submit', 'contact-form-7' );

	}

	$atts['type'] = 'submit';
	$atts = wpcf7_format_atts( $atts );
	$html = sprintf( '<button class="btn">%2$s</button>', $atts, $value );

	return $html;

}

wpcf7_add_form_tag( 'submit', __NAMESPACE__ . '\wpcf7_submit_button_tag_handler', true ); /* [ submit submit ] */

/**
 * ===========================================================================================================================================
 * Redirect singles back to archive page
 *
 */
function redirect( $url ) {
  ob_start();
  header( 'Location: ' . $url );
  ob_end_flush();
  die();
}
