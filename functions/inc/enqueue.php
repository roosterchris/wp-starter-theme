<?php
/**
 * Enqueue scripts and styles functions for the CLIENTNAME website theme
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFSLUG;

/**
 * ===========================================================================================================================================
 * Loading scripts Asyncronously
 *
 * Append #asyncload to the url of an enqueued script.
 *
 * This will then strip the #asynload from the url and add async="async" to the
 * script tag.
 *
 * @param string $url The url of the script.
 */
function async_scripts( $url ) {
	if ( strpos( $url, '#asyncload' ) === false ) {

		return $url;

	} elseif ( is_admin() ) {

		return str_replace( '#asyncload', '', $url );

	} else {

		return str_replace( '#asyncload', '', $url ) . "' async='async";

	}
}
add_filter( 'clean_url', __NAMESPACE__ . '\async_scripts', 11, 1 );

/**
 * ===========================================================================================================================================
 * Enqueue scripts and styles.
 */
function scripts() {
	wp_enqueue_style( 'style', get_stylesheet_uri(), array(), 1.0 );

	wp_register_style( 'stylesheet', get_template_directory_uri() . '/dist/css/styles.css', array(), 1.0 );
	wp_enqueue_style( 'stylesheet' );

	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', get_template_directory_uri() . '/dist/js/jquery.min.js', false, '3.5.1' );
	wp_enqueue_script( 'jquery' );

	/**
   * Add in fonts here
   *
   * For Example:
   * wp_register_style( 'adobeFonts', 'https://use.typekit.net/arr4ruq.css',false, false );
	 * wp_enqueue_style( 'adobeFonts' );
   */

	wp_register_script( 'fontAwesome', 'https://kit.fontawesome.com/5d1981e9cc.js#asyncload', false, false, false );
  wp_enqueue_script( 'fontAwesome' );

	wp_register_script( 'modernizr', get_template_directory_uri() . '/dist/js/modernizr.js', array(), '1.0.0', true );
	wp_enqueue_script( 'modernizr' );

	if ( is_page_template( 'templates/contact-template.php' ) ) {
  	wp_register_script( 'google-maps-API', 'https://maps.googleapis.com/maps/api/js?key=' . esc_attr( google_api_key() ) . '', array(), '1.0.0', true );
  	wp_enqueue_script( 'google-maps-API' );
  }

	wp_register_script( 'bundle', get_template_directory_uri() . '/dist/js/libraries/bundle.js', array(), '1.0.0', true );
	wp_enqueue_script( 'bundle' );

	wp_register_script( 'custom', get_template_directory_uri() . '/dist/js/custom.js', array(), '1.0.0', true );
	wp_enqueue_script( 'custom' );

	wp_enqueue_script( 'navigation', get_template_directory_uri() . '/dist/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'skip-link-focus-fix', get_template_directory_uri() . '/dist/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\scripts' );
