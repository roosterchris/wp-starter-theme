<?php
/**
 * Custom Login functions for the CLIENTNAME website theme
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFSLUG;

/**
 * ===========================================================================================================================================
 * Change the login WP logo to a custom logo
 */
function my_login_logo() { ?>
  <style type="text/css">
  body.login {
		/* add styles for the background of the page */
  }

  #login h1, .login h1 {
		padding: 10px;
  }

  #login h1 a, .login h1 a {
		background-image: url( <?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/img/logo.svg );
		background-repeat: no-repeat;
		background-size: 300px 70px;
		height: 70px;
		margin-bottom: 30px;
		padding-bottom: 0px;
		width: 300px;
  }

  .login #login_error, .login .message {
		/* add styles for the messages */
  }

  body.login form {
		/* add styles for the form */
  }

  .login #backtoblog a, .login #nav a {
		/* add styles for the links */
  }

  body.login.wp-core-ui .button.button-large {
		/* add styles for the button */
		transition: all ease 0.3s;
			-webkit-transition: all ease 0.3s;
  }

  body.login.wp-core-ui .button.button-large:hover {
		/* add styles for the button hover */
  }
  </style>

	<?php
}
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\my_login_logo' );

/**
 * ===========================================================================================================================================
 * Change the login WP logo url
 */
function my_login_logo_url() {
	return home_url();
}
add_filter( 'login_headerurl', __NAMESPACE__ . '\my_login_logo_url' );

/**
 * ===========================================================================================================================================
 * Change the login WP logo url title
 */
function my_login_logo_url_title() {
	return 'CLIENTNAME';
}
add_filter( 'login_headertext', __NAMESPACE__ . '\my_login_logo_url_title' );
