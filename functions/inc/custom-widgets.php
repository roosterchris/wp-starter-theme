<?php
/**
 * Custom Widgets functions for the CLIENTNAME website theme
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFSLUG;

/**
 * ===========================================================================================================================================
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'NAMEOFTHEME' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'NAMEOFTHEME' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', __NAMESPACE__ . '\widgets_init' );

/**
 * ===========================================================================================================================================
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function add_dashboard_widgets() {
	wp_add_dashboard_widget(
		'dashboard_widget',         // Widget slug.
		'Welcome to your website!', // Title.
		__NAMESPACE__ . '\dashboard_widget_function' // Display function.
	);
}
add_action( 'wp_dashboard_setup', __NAMESPACE__ . '\add_dashboard_widgets' );


/**
 * ===========================================================================================================================================
 * Create the function to output the contents of our Dashboard Widget
 */
function dashboard_widget_function() { ?>
  <style type="text/css">
		.customdash-half {
			width: 49%;
			display: inline-block;
			vertical-align: middle;
		}

		.customdash-half img{
			float: right;
			width: 200px;
			max-width: 100%;
			margin: 10px 0 0 20px;
		}
  </style>

	<?php
	// Display whatever it is you want to show.
		echo 'Hello and welcome to your website dashboard. If you have a question or need help with your website, please get in touch';
	?>

  <br>

	<ul class="customdash-half">
		<li><b>Contact us:</b> <a href="https://www.roostermarketing.com/contact" target="blank">click here</a></li>
	</ul>

  <div class="customdash-half">
		<a href="https://www.roostermarketing.com" target="blank"><img style="width: 40px" src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/dist/img/rooster-logo.svg" /></a>
  </div>

	<?php
}
