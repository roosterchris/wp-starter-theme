<?php
/**
 * Custom Browser functions for the CLIENTNAME website theme
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFSLUG;

/**
 * ===========================================================================================================================================
 * Wordpress Browser Classes
 *
 * Defining device variables
 *
 * Device - iPad
 */
function is_ipad() {
	$is_ipad = ( bool ) strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'iPad' );

	if ( $is_ipad ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Device - iPhone
 */
function is_iphone() {
	$cn_is_iphone = ( bool ) strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'iPhone' );
	if ( $cn_is_iphone ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Devicce - iPod
 */
function is_ipod() {
	$cn_is_iphone = ( bool ) strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'iPod' );
	if ( $cn_is_iphone ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Device - iOS
 */
function is_ios() {
	if ( is_iphone() || is_ipad() || is_ipod() ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Device - Android
 */
function is_android() {
	$is_android = ( bool ) strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Android' );
	if ( $is_android ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Device - Android Mobile
 */
function is_android_mobile() {
	$is_android   = ( bool ) strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Android' );
	$is_android_m = ( bool ) strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Mobile' );
	if ( $is_android && $is_android_m ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Device - Android Tablet
 */
function is_android_tablet() {
	if ( is_android() && ! is_android_mobile() ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Device - All Mobile Devices
 */
function is_mobile_device() {
	if ( is_android_mobile() || is_iphone() || is_ipod() ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Device - All Tablets
 */
function is_tablet() {
	if ( ( is_android() && ! is_android_mobile() ) || is_ipad() ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Add the class to body for different browsers and devices.
 *
 * @param string $classes Classes for different browsers and devices.
 */
function body_class( $classes ) {
	global $is_gecko, $is_IE, $is_opera, $is_safari, $is_chrome, $is_iphone, $is_edge;

	if ( ! wp_is_mobile() ) {
		if ( $is_gecko ) {

			$classes[] = 'firefox';

		} elseif ( $is_opera ) {

			$classes[] = 'opera';

		} elseif ( $is_safari ) {

			$classes[] = 'safari';

		} elseif ( $is_chrome ) {

			$classes[] = 'chrome';

		} elseif ( $is_edge ) {

			$classes[] = 'edge';

		} elseif ( $is_IE ) {

			$classes[] = 'ie';

			if ( preg_match( '/MSIE ( [ 0-9 ]+ )( [ a-zA-Z0-9. ]+ )/', wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), $browser_version ) ) {
				$classes[] = 'ie' . $browser_version[1];
			}
		} else {

			$classes[] = 'browser-unknown';

		}
	} else {
		if ( is_iphone() ) {

			$classes[] = 'iphone';

		} elseif ( is_ipad() ) {

			$classes[] = 'ipad';

		} elseif ( is_ipod() ) {

			$classes[] = 'ipod';

		} elseif ( is_android() ) {

			$classes[] = 'android';

		} elseif ( is_tablet() ) {

			$classes[] = 'device-tablet';

		} elseif ( is_mobile_device() ) {

			$classes[] = 'device-mobile';

		} elseif ( strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Kindle' ) !== false ) {

			$classes[] = 'browser-kindle';

		} elseif ( strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'BlackBerry' ) !== false ) {

			$classes[] = 'browser-blackberry';

		} elseif ( strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Opera Mini' ) !== false ) {

			$classes[] = 'browser-opera-mini';

		} elseif ( strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Opera Mobi' ) !== false ) {

			$classes[] = 'browser-opera-mobi';

		}
	}

	if ( strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Windows' ) !== false ) {
		$classes[] = 'os-windows';

	} elseif ( is_android() ) {

		$classes[] = 'os-android';

	} elseif ( is_ios() ) {

		$classes[] = 'os-ios';

	} elseif ( strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Macintosh' ) !== false ) {

		$classes[] = 'os-mac';

	} elseif ( strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Linux' ) !== false ) {

		$classes[] = 'os-linux';

	} elseif ( strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'Kindle' ) !== false ) {

		$classes[] = 'os-kindle';

	} elseif ( strpos( wp_unslash( isset( $_SERVER['HTTP_USER_AGENT'] ) ), 'BlackBerry' ) !== false ) {

		$classes[] = 'os-blackberry';

	}

	return $classes;
}
add_filter( 'body_class', __NAMESPACE__ . '\body_class' );
