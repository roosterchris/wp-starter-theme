<?php
/**
 * Wordpress Backend functions for the CLIENTNAME website theme
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFSLUG;

/**
 * ===========================================================================================================================================
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function setup() {

	/**
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 */

	load_theme_textdomain( 'NAMEOFTHEME', get_template_directory() . '/languages' );

	/**
   * Add default posts and comments RSS feed links to head.
   */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/**
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	/**
   * Enable Custom Image Sizes to be created within the Media Library.
   */
	add_image_size( 'xxxxl', 3840, 3840 );
	add_image_size( 'xxxl', 2400, 2400 );
	add_image_size( 'xxl', 1920, 1920 );
	add_image_size( 'xl', 1400, 1400 );
	add_image_size( 'l', 1200, 1200 );
	add_image_size( 'm', 768, 768 );
	add_image_size( 's', 514, 514 );
	add_image_size( 'xs', 320, 320 );

	/**
   * Increase the 1600px WordPress limit for images included in 'srcset' attributes.
   */
	add_filter(
		'max_srcset_image_width',
		function( $max_srcset_image_width, $size_array ) {
			return 3840;
		},
		10,
		2
	);

	/**
   * This theme uses wp_nav_menu() in one location.
   */
	register_nav_menus(
		array(
			'main-menu' => esc_html__( 'Primary', 'NAMEOFTHEME' ),
		)
	);

	/**
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

	/**
   * Add theme support for selective refresh for widgets.
   */
	add_theme_support( 'customize-selective-refresh-widgets' );

}
add_action( 'after_setup_theme', __NAMESPACE__ . '\setup' );

/**
 * ===========================================================================================================================================
 * Enqueue scripts and styles for this theme
 */
require get_template_directory() . '/functions/inc/enqueue.php';


/**
 * ===========================================================================================================================================
 * Custom template tags for this theme.
 */
require get_template_directory() . '/functions/inc/template-tags.php';

/**
 * ===========================================================================================================================================
 * Advanced Custom Fields Options Pages
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	$option_page = acf_add_options_page(
		array(
			'page_title' 	=> 'Theme General Settings',
			'menu_title' 	=> 'Theme Settings',
			'menu_slug' 	=> 'theme-general-settings',
			'capability' 	=> 'edit_posts',
			'redirect' 		=> false,
		)
	);
}

/**
 * ===========================================================================================================================================
 * Change the WP Admin Footer
 */
function remove_footer_admin() {
	echo 'Built with ♥ by Rooster Marketing';
}
add_filter( 'admin_footer_text', __NAMESPACE__ . '\remove_footer_admin' );
add_action( 'admin_bar_menu', __NAMESPACE__ . '\remove_wp_logo', 999 );


/**
 * ===========================================================================================================================================
 * Remove the WP Admin Logo from the Top Admin Bar
 *
 * @param string $wp_admin_bar The admin bar.
 */
function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}

/**
 * ===========================================================================================================================================
 * Remove Admin Dashboard Widgets
 */
function remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts'] );
	unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_primary'] );
	unset( $wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary'] );

	if ( defined( 'WPSEO_VERSION' ) ) {
		remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );
	}
}
add_action( 'wp_dashboard_setup', __NAMESPACE__ . '\remove_dashboard_widgets' );
remove_action( 'welcome_panel', __NAMESPACE__ . '\wp_welcome_panel' );

/**
 * ===========================================================================================================================================
 * Move Yoast to the bottom
 */
function yoast_to_bottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', __NAMESPACE__ . '\yoast_to_bottom' );

/**
 * ===========================================================================================================================================
 * Add button to the WYSIWYG
 *
 * Hooks your functions into the correct filters
 */
function add_mce_button() {
	// check user permissions.
	if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
		return;
	}
	// check if WYSIWYG is enabled.
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', __NAMESPACE__ . '\add_tinymce_plugin' );
		add_filter( 'mce_buttons', __NAMESPACE__ . '\register_mce_button' );
	}
}
add_action( 'admin_init', __NAMESPACE__ . '\add_mce_button' );

/**
 * Register the new button in the editor
 *
 * @param string $buttons The button in the WYSIWYG.
 */
function register_mce_button( $buttons ) {
	array_push( $buttons, 'rooster_mce_button' );
	return $buttons;
}

/**
 * Declare a script for the new button
 *
 * The script will insert the shortcode on the click event
 *
 * @param string $plugin_array The path to the button script.
 */
function add_tinymce_plugin( $plugin_array ) {
	$plugin_array['rooster_mce_button'] = get_template_directory_uri() . '/dist/js/mce.js';
	return $plugin_array;
}

/**
 * ===========================================================================================================================================
 * Add the stylesheet to the WYSIWYG
 */
function theme_add_editor_styles() {
	add_editor_style( 'style.css' );
}
add_action( 'admin_init', __NAMESPACE__ . '\theme_add_editor_styles' );

/**
 * ===========================================================================================================================================
 * Conditionally load CF7 & Google reCAPTCHA CSS and JS files
 */
function conditionally_load_plugin_js_css() {
	// Page ID for the page that you want to show the contact form on.
	$contact_id = 14;
	if ( ! is_page( $contact_form_page ) ) {
		wp_dequeue_script( 'contact-form-7' );
		wp_dequeue_script( 'google-recaptcha' );
		wp_dequeue_style( 'contact-form-7' );
	}
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\conditionally_load_plugin_js_css' );
