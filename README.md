## CLIENTNAME

### Tasks to make the theme ready to use

Do a find and replace for the project in Atom for the following

- `NAMEOFTHEME`
- `CLIENTNAME`
- `NAMEOFSLUG`
- `SITESLUG`

Open terminal and `cd PATH/TO/THEME`

Install npm modules

- `npm install`

Install Composer WordPress Standards

- `composer create-project wp-coding-standards/wpcs --no-dev && composer install`

Setup gulp

```
gulp vendor
gulp imagemin
gulp js:dev
gulp concat:js
gulp
```

You're now good to go.
