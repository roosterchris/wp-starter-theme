
if( navigator.userAgent.match( /Trident.*rv:11\./ ) ) {
  $( 'body' ).addClass( 'ie11' );
}

var themePath = '/wp-content/themes/NAMEOFTHEME/';

// near the top of the file
var map = null; // For ACF google maps

//ACF MAP FUNCTIONS
function newMap ($el) {
    // var
    var $markers = $el.find('.marker');
    // vars
    var snazzy = ( function () {
        var json = null;
        $.ajax({
            'async': false,
            'global': false,
            'url': themePath + 'conf/snazzy.json',
            'dataType': 'json',
            'success': function (data) {
                json = data;
            },
        });
        return json;
    })();
    // vars
    var args = {
        zoom: 15,
        center: new google.maps.LatLng(0, 0),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        styles: snazzy,
    };

    // create map
    var map = new google.maps.Map($el[0], args);
    // add a markers reference
    map.markers = [];
    // add markers
    $markers.each(function () {
        addMarker($(this), map);
    });
    // center map
    centerMap(map);
    // Add loaded class to body
    $( 'body' ).addClass( 'js-map-loaded' );
    // return
    return map;
}

//GOOGLE MAP MARKER
function addMarker ( $marker, map ) {
    // var
    var latlng = new google.maps.LatLng( $marker.attr( 'data-lat' ), $marker.attr( 'data-lng' ) );

    var mapMarkerIcon = themePath + 'dist/images/map-marker.png';


    // create marker
    var icon = {url:mapMarkerIcon, size: new google.maps.Size(38, 48)};
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: icon,

    });
    // add to array
    map.markers.push( marker );
    // if marker contains HTML, add it to an infoWindow
    if ( $marker.html() ) {
        // create info window
        var info = null;
        var closeDelayed = false;
        var closeDelayHandler = function() {
          $(info.getWrapper()).removeClass( 'active' );
          setTimeout( function() {
            closeDelayed = true;
            info.close();
          }, 300);
        };
        var info = new SnazzyInfoWindow({
          marker: marker,
          content: $marker.html(),
          border: false,
          borderRadius: '0px',
          shadow: true,
          panOnOpen: true,
          closeWhenOthersOpen: true,
          placement: 'bottom',
          closeOnMapClick: true,
          closeButtonMarkup: '<i class="fal fa-times close"></i>',
          callbacks: {
            open: function() {
              $( this.getWrapper() ).addClass( 'open' );
            },
            afterOpen: function() {
              var wrapper = $( this.getWrapper() );
              wrapper.addClass( 'active' );
              wrapper.find( '.close' ).on( 'click', closeDelayHandler );
            },
            beforeClose: function() {
              if ( !closeDelayed ) {
                closeDelayHandler();
                return false;
              }
              return true;
            },
            afterClose: function() {
              var wrapper = $(this.getWrapper());
              wrapper.find('.close').off();
              wrapper.removeClass('open');
              closeDelayed = false;
            }
          }
        });

        // show info window when marker is clicked
        google.maps.event.addListener(marker, 'click', function() {
          info.open(map, marker);
        });

        info.open(map,marker);

    }
}

//CENTER THE MAP
function centerMap ( map ) {
    // vars
    var bounds = new google.maps.LatLngBounds();
    // loop through all markers and create bounds
    $.each( map.markers, function ( i, marker ) {
        var latlng = new google.maps.LatLng( ( marker.position.lat() - 0.005 ), marker.position.lng() );
        bounds.extend( latlng );
    });
    // If there's no marker for any reason centre on Southampton
    if ( map.markers.length < 1 ) {
        var latlng = new google.maps.LatLng( 50.907190, -1.412997 );
        bounds.extend( latlng );
    }
    map.setCenter( bounds.getCenter() );
}

//fade in the map when page has loaded to prevent infowindow content clitch
 $( window ).on( 'load', function () {
  $( '.acf-map' ).each( function() {
    if ( $( '.acf-map' ).length > 0 ) {
      $( '.acf-map' ).animate( {opacity: 1}, 600 );
    }
  });
});

//Document on load functions
$( function() {
  //initialize object fit images plugin
  objectFitImages();

  //ACF GOOGLE MAPS
  // $('.acf-map').each(function () {
  //     // create map
  //     map = newMap($(this));
  //     google.maps.event.trigger(map, 'resize');
  // });

  $( '.acf-map' ).each( function() {
		if ( $( '.acf-map' ).length > 0 ) {
			newMap( $( this ) );
		}
	});


	//SHOW MENU
  $( '.menu-toggle' ).click( function() {
    if ( $( '#mobile-navigation' ).hasClass( 'show' ) ) {
      $( '#mobile-navigation' ).removeClass( 'show' );
      $( this ).removeClass( 'open' );
      $( '#masthead' ).removeClass( 'menu-open' );
      setTimeout( function() {
        $( '#mobile-navigation' ).removeClass( 'displayBlock' );
      }, 500 );
    }
    else {
      $( this ).addClass( 'open' );
      $( '#mobile-navigation' ).addClass( 'displayBlock' ).outerWidth();
      $( '#masthead' ).addClass( 'menu-open' );
      $( '#mobile-navigation' ).addClass('show' );
    }
  });

  // MOBILE SUB MENU ACTIVATION
  $( '#mobile-navigation ul li.menu-item-has-children' ).prepend( '<div class="menu-expand"><i class="fas fa-sort-down"></i></div>' );
  $( '.menu-expand' ).click( function() {
    $( this ).toggleClass( 'active' );
    $( '.menu-expand i' ).toggleClass( 'fa-sort-down fa-sort-up' );
    $( this ).parent('li' ).find( '> ul.sub-menu' ).slideToggle( 300 );
  });

  // Hide Header on on scroll down
  var didScroll;
  var lastScrollTop = 0;
  var delta = 5;
  var navbarHeight = $( '#masthead' ).outerHeight();

  //MENU SCROLL RESIZE
  $( window ).scroll( function() {
    var scroll = $( window ).scrollTop();

    if ( scroll >= 50 ) {
      $( '#masthead' ).addClass( 'smaller' );
    } else {
      $( '#masthead' ).removeClass( 'smaller' );
    }
  });


  $( window ).scroll( function( event ){
    didScroll = true;
  });

  setInterval( function() {
    if ( didScroll ) {
      hasScrolled();
      didScroll = false;
    }
  }, 150);

  function hasScrolled() {
    var st = $( this ).scrollTop();

    // Make sure they scroll more than delta
    if( Math.abs( lastScrollTop - st ) <= delta )
      return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if ( st > lastScrollTop && st > navbarHeight ) {
      // Scroll Down
      $( '#masthead' ).removeClass( 'nav-down' ).addClass( 'nav-up' );

    } else {

      // Scroll Up
      if( st + $( window ).height() < $( document ).height() ) {
        $( '#masthead' ).removeClass( 'nav-up' ).addClass( 'nav-down' );
      }
    }

    lastScrollTop = st;
  }


  //ANIMATIONS
  wow = new WOW(
    {
    boxClass:     'anim',      // default
    animateClass: 'animated', // default
    offset:       0,          // default
    mobile:       true,       // default
    live:         true        // default
    }
  )
  wow.init();

	// SMOOTH SCROLLING FROM ANOTHER PAGE TO CORRECT ID
	//START AT TOP OF PAGE AND SCROLL DOWN ALWAYS
  if ( window.location.hash ) {
    setTimeout( function() {
      $( 'html, body' ).scrollTop( 0 ).show();
      $( 'html, body' ).animate( {
        scrollTop: $( window.location.hash ).offset().top-0
        }, 1000 )
    }, 0 );
  }
  else {
    $( 'html, body' ).show();
  }

  //SMOOTH SCROLLING
	// Select all links with hashes
	$( 'a[href*="#"]' )
  // Remove links that don't actually link to anything
  .not( '[href="#"]' )
  .not( '[href="#0"]' )
  .click( function( event ) {
    // On-page links
    if (
      location.pathname.replace( /^\//, '' ) == this.pathname.replace( /^\//, '' )
      &&
      location.hostname == this.hostname
    ) {
		// Figure out element to scroll to
		var target = $( this.hash );
		target = target.length ? target : $( '[name=' + this.hash.slice( 1 ) + ']' );
		// Does a scroll target exist?
		if ( target.length ) {
			// Only prevent default if animation is actually gonna happen
			event.preventDefault();
			$( 'html, body' ).animate({
			  scrollTop: target.offset().top-0
			}, 1000, function() {
		  // Callback after animation
		  // Must change focus!
		  });
     }
    }
  });
});
