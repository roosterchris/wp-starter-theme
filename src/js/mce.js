( function() {
  tinymce.PluginManager.add( 'rooster_mce_button', function( editor, url ) {
    editor.addButton( 'rooster_mce_button', {
      text: 'Button',
      icon: false,
      onclick: function() {
      // change the shortcode as per your requirement
        editor.insertContent( '<a class="button">Read more</a>' );
      }
    } );
  } );
} )();
