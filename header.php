<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <main id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NAMEOFTHEME
 */

 use Rooster\NAMEOFSLUG as Theme;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
  <!-- Favicon links go here -->

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'NAMEOFTHEME' ); ?></a>

		<header id="masthead" class="section">

			<div class="container ph">
				<div class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<span class="line line__top"></span>
					<span class="line line__center"></span>
					<span class="line line__bottom"></span>
				</div>

				<div class="site-branding">
					<a href="<?php echo esc_url( home_url() ); ?>">
						<?php Theme\print_svg_file( get_template_directory() . '/dist/img/logo.svg' ); ?>
					</a>
				</div><!-- .site-branding -->

				<nav id="site-navigation" class="main-navigation">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'main-menu',
							'menu_id'        => 'main-menu',
						)
					);
					?>
				</nav><!-- #site-navigation -->

				<div class="social">
					<?php if ( get_field( 'facebook', 'options' ) ) : ?>
						<a href="<?php the_field( 'facebook', 'options' ); ?>" target="blank">
						<i c	lass="fab fa-facebook-f"></i>
						</a>
					<?php endif; ?>

					<?php if ( get_field( 'instagram', 'options' ) ) : ?>
						<a href="<?php the_field( 'instagram', 'options' ); ?>" target="blank">
						<	i class="fab fa-instagram"></i>
						</a>
					<?php endif; ?>

					<?php if ( get_field( 'linkedin', 'options' ) ) : ?>
						<a href="<?php the_field( 'linkedin', 'options' ); ?>" target="blank">
						<i	 class="fab fa-linkedin-in"></i>
						</a>
					<?php endif; ?>

					<?php if ( get_field( 'twitter', 'options' ) ) : ?>
						<a href="<?php the_field( 'twitter', 'options' ); ?>" target="blank">
						<i c	lass="fab fa-twitter"></i>
						</a>
					<?php endif; ?>
				</div>
			</div>

			<nav id="mobile-navigation" class="main-navigation">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'main-menu',
						'menu_id'        => 'main-menu',
					)
				);
				?>
			</nav><!-- #mobile-navigation -->

		</header><!-- #masthead -->

		<div id="content" class="site-content">
