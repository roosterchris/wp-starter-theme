<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NAMEOFTHEME
 */

 use Rooster\NAMEOFSLUG as Theme;

?>

</div><!-- #content -->

<footer id="footer" class="section">

	<div class="container ph pv textcenter">
		<div class="social">
			<?php if ( get_field( 'facebook', 'options' ) ) : ?>
				<a href="<?php the_field( 'facebook', 'options' ); ?>" target="blank">
					<i class="fab fa-facebook-f"></i>
				</a>
			<?php endif; ?>

			<?php if ( get_field( 'instagram', 'options' ) ) : ?>
				<a href="<?php the_field( 'instagram', 'options' ); ?>" target="blank">
					<i class="fab fa-instagram"></i>
				</a>
			<?php endif; ?>

			<?php if ( get_field( 'linkedin', 'options' ) ) : ?>
				<a href="<?php the_field( 'linkedin', 'options' ); ?>" target="blank">
					<i class="fab fa-linkedin-in"></i>
				</a>
			<?php endif; ?>

			<?php if ( get_field( 'twitter', 'options' ) ) : ?>
				<a href="<?php the_field( 'twitter', 'options' ); ?>" target="blank">
					<i class="fab fa-twitter"></i>
				</a>
			<?php endif; ?>
		</div>
	</div>

	<div class="container ph copyright">
		<div class="left">
			&copy; <?php echo esc_html( gmdate( 'Y' ) ); ?> <?php bloginfo(); ?>. <?php the_field( 'copyright', 'options' ); ?>

			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'legal-menu',
					'menu_id'        => 'legal-menu',
				)
			);
			?>
		</div>

		<div class="right">
			Design & Build by <a href="https://www.roostermarketing.com/" target="_blank">Rooster Marketing</a>
		</div>
	</div>

</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<!-- IE9 NOTICE -->
<!--[if lte IE 9]>
	<div class="legacy-browser-notice">
		<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/dist/img/ie-logo.png" />
		<p class="notice">Important Notice</p>
		<p>You are using an <strong>out of date</strong> version of Internet Explorer!
		This browser may prevent this website from displaying and functioning as intended.</p>
		<p>Please <a href="http://www.browsehappy.com" target="_blank">click here</a> to upgrade.</p>
	</div>
<![endif]-->

</body>
</html>
