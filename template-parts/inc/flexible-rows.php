<?php
/**
 * Template part for displaying Flexible Rows on Flexible Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NAMEOFTHEME
 */

 use Rooster\NAMEOFSLUG as Theme;


if ( get_row_layout() == 'text' ) :

	include( locate_template( 'template-parts/inc/text.php' ) );

endif;
