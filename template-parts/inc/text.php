<?php
/**
 * Template part for displaying Text row on Flexible Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NAMEOFTHEME
 */

 use Rooster\NAMEOFSLUG as Theme;

$padding = get_sub_field( 'padding' );
$text = get_sub_field( 'text' );
?>

<section id="<?php echo $section_id; ?>" class="default-content <?php echo $padding; ?>">
	<div class="container ph">
		<div class="text anim fadeIn">
			<?php echo $text; ?>
		</div>
	</div>
</section>
