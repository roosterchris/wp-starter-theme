<?php
/**
 * Template part for displaying Flexible Content on Flexible Template
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NAMEOFTHEME
 */

 use Rooster\NAMEOFSLUG as Theme;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="flexible-wrap">

		<?php

		// check if the flexible content field has rows of data.
		if ( have_rows( 'flexible_content' ) ) :
			// loop through the rows of data.
			while ( have_rows( 'flexible_content' ) ) :
				the_row();

				include( locate_template( 'template-parts/inc/flexible-rows.php' ) );

			endwhile;

		else :

			// no layouts found.
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</div>
</article><!-- #post-<?php the_ID(); ?> -->
